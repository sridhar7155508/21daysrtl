// Code your testbench here
// or browse Examples
module day9_tb();
  localparam VEC_W = 5;
  
  logic[VEC_W-1:0] bin_i;
  logic[VEC_W-1:0] gray_o;
  
  day9 #(VEC_W) DAY9(.*);
  
  initial begin
    for(int i = 0; i < 2**VEC_W; i++) begin
      bin_i = i;
      #5;
    end
  end
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars();
  end
endmodule
