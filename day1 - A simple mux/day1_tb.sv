module day1_tb;
    logic[7:0]  a_i;
    logic[7:0]  b_i;
    logic       sel_i;
    logic[7:0]  y_o;

    day1 DUT(.*);

    initial begin
        for(int i = 0; i < 4; i++) begin
            a_i     = $urandom_range(0, 255);
            b_i     = $urandom_range(0, 255);
            sel_i   = $urandom;
            #5;
        end
        $finish();
    end

endmodule
