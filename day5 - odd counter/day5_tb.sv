// Code your testbench here
// or browse Examples
module day5_tb();
  logic clk;
  logic reset;
  
  logic[7:0]	cnt_o;
  
  day5 DAY5(.*);
  
  always begin
    clk = 1'b1;
    #5;
    clk = 1'b0;
    #5;
  end
  
  initial begin
    reset = 1'b1;
    @(posedge clk);
    reset = 1'b0;
    for(int i = 0; i < 256; i++) begin 
      @(posedge clk);
    end
    $finish();
  end
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars();
  end
endmodule
