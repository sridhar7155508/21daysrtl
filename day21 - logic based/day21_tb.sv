module day21_tb();
  
  localparam WIDTH = 4;
  
  logic[WIDTH-1:0] vec_i;
  logic[WIDTH-1:0] second_bit_o;
  
  day21 #(WIDTH) DUT (.*);
  
  initial begin
    for(int i=0; i<16; i++) begin
      vec_i = $urandom_range(0, 2**WIDTH - 1);
      #5;
    end
    $finish();
  end
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars();
  end
  
endmodule