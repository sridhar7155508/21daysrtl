// Code your testbench here
// or browse Examples
module day10_tb();
  logic clk;
  logic reset;
  logic load_i;
  
  logic[3:0] load_val_i;
  logic[3:0] count_o;
  
  day10 DAY10(.*);
  
  int cycles;
  
  always begin
    clk = 1'b1;
    #5;
    clk = 1'b0;
    #5;
  end
  
  initial begin
    reset = 1'b1;
    load_i = 0;
    load_val_i = 4'h0;
    repeat(2) @(posedge clk);
    reset = 1'b0;
    @(posedge clk);
    for(int i = 0; i < 3; i++) begin
      load_i <= 1;
      load_val_i <= 3*i;
      cycles = 4'hF - load_val_i[3:0];
      @(posedge clk);
      load_i <= 0;
      while(cycles) begin
        cycles = cycles - 1;
        @(posedge clk);
      end
    end
    #40 $finish();
  end
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars();
  end
endmodule
