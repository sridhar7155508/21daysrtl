module day7(
    input   wire        clk,
    input   wire        reset,

    output  wire[3:0]   lfsr_o
);

    logic[3:0] lfsr_ff;
    logic[3:0] lfsr_nxt;

    always_ff @(posedge clk or posedge reset) begin
        if(reset) begin
            lfsr_ff <= 4'h1;
        end
        else begin
            lfsr_ff <= lfsr_nxt;
        end
    end

    assign lfsr_nxt     = {lfsr_ff[2:0], lfsr_ff[1] ^ lfsr_ff[3]};
    assign lfsr_o       = lfsr_ff;
endmodule
