module day18_tb();
  logic			clk;
  logic			reset;
  
  logic			psel_i;
  logic			penable_i;
  logic			pwrite_i;
  logic[9:0]	paddr_i;
  logic[31:0]	pwdata_i;
  
  logic			pready_o;
  logic[31:0]	prdata_o;
  
  day18 DAY18(.*);
  
  always begin
    clk = 1'b1;
    #5;
    clk = 1'b0;
    #5;
  end
    
  logic[9:0][9:0] addr_list;
  int i1, i2;
  initial begin
    reset 		= 1'b0;
    psel_i		= 1'b0;
    penable_i	= 1'b0;
    repeat(3) @(posedge clk);
    reset 		= 1'b1;
    repeat(3) @(posedge clk);
    reset		= 1'b0;
    repeat(3) @(posedge clk);
    for(i1=0; i1<5; i1++) begin
      psel_i		= 1'b1;
      penable_i		= 1'b1;
      paddr_i		= $urandom_range(0, 1023);
      pwrite_i		= 1'b1;
      pwdata_i		= $urandom_range(0, 32'hFFFF);
      addr_list[i1]	= paddr_i;
      while(~pready_o) 
        @(posedge clk);
      psel_i		= 1'b0;
      penable_i		= 1'b0;
      @(posedge clk);      
    end
    $display("%0t", $time);
    //repeat(5) @(posedge clk);
    for(i2=0; i2<5; i2++) begin
      psel_i		= 1'b1;
      penable_i		= 1'b1;
      paddr_i		= addr_list[i2];
      pwrite_i		= 1'b0;
      while(~pready_o) 
        @(posedge clk);
      psel_i		= 1'b0;
      penable_i		= 1'b0;
      @(posedge clk);      
    end
    $finish();
  end
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars();
    //repeat(91) @(posedge clk);
    //$finish();    
  end
  
endmodule