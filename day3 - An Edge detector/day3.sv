module day3(
    input       wire    clk,
    input       wire    reset,

    input       wire    a_i,

    output      wire    rising_edge_o,
    output      wire    falling_edge_o
);

    logic   a_ff;

    always_ff @(posedge clk or posedge reset) begin
        if(reset) begin
            a_ff    <= 0;
        end
        else begin
            a_ff    <= a_i;
        end
    end

    assign rising_edge_o    = a_i & !a_ff;
    assign falling_edge_o   = !a_i & a_ff;

endmodule
