module day17_tb ();

  logic        clk;
  logic        reset;
  logic        req_i;
  logic        req_rnw_i;
  logic[9:0]   req_addr_i;
  logic[31:0]  req_wdata_i;
  logic        req_ready_o;
  logic[31:0]  req_rdata_o;

  // Instatiate the RTL
  day17 DAY17 (.*);

  logic [9:0] [9:0] addr_list;

  // Generate the clock
  always begin
    clk = 1'b1;
    #5;
    clk = 1'b0;
    #5;
  end

  // Generate stimulus
  int txn, txn1;
//   initial begin
//     reset <= 1'b1;
//     req_i <= 1'b0;
//     @(posedge clk);
//     reset <= 1'b0;
//     repeat(5) @(posedge clk);
//     for (txn=0; txn<2; txn++) begin
//       // Write 10 transactions
//       req_i       <= 1'b1;
//       req_rnw_i   <= 0;
//       req_addr_i  <= $urandom_range(0, 1023);
//       addr_list[txn] <= req_addr_i;
//       req_wdata_i <= $urandom_range(0, 32'hFFFF);
//       $display("%0t index : %0d addr: %0h wdata : %0h", $time, txn, req_addr_i, req_wdata_i );
//       // Wait for ready
//       while (~req_ready_o) begin
//         @(posedge clk);
//       end
//       req_i <= 1'b0;
//       @(posedge clk);
//     end
//     for (txn1=0; txn1<2; txn1++) begin
//       // Read 10 transactions
//       req_i       <= 1'b1;
//       req_rnw_i   <= 1;
//       req_addr_i  <= addr_list[txn1];
//       $display("%0t index : %0d addr: %0h rdata : %0h", $time, txn1, req_addr_i, req_rdata_o );
//       // Wait for ready
//       while (~req_ready_o) begin
//         @(posedge clk);
//       end
//       req_i <= 1'b0;
//       @(posedge clk);
//     end
//     $finish();
//   end
  initial begin
    reset = 1'b1;
    req_i = 1'b0;
    @(posedge clk);
    reset = 1'b0;
    repeat(5) @(posedge clk);
    for (txn=0; txn<2; txn++) begin
      // Write 10 transactions
      req_i       = 1'b1;
      req_rnw_i   = 0;
      req_addr_i  = $urandom_range(0, 1023);
      addr_list[txn] = req_addr_i;
      req_wdata_i = $urandom_range(0, 32'hFFFF);
      $display("%0t index : %0d addr: %0h wdata : %0h", $time, txn, req_addr_i, req_wdata_i );
      // Wait for ready
      while (~req_ready_o) begin
        @(posedge clk);
      end
      req_i = 1'b0;
      @(posedge clk);
    end
    for (txn1=0; txn1<2; txn1++) begin
      // Read 10 transactions
      req_i       = 1'b1;
      req_rnw_i   = 1;
      req_addr_i  = addr_list[txn1];
      $display("%0t index : %0d addr: %0h rdata : %0h", $time, txn1, req_addr_i, req_rdata_o );
      // Wait for ready
      while (~req_ready_o) begin
        @(posedge clk);
      end
      req_i = 1'b0;
      @(posedge clk);
    end
    $finish();
  end
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars();
  end
  
endmodule