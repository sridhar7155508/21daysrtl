// Code your design here
module day6(
    input   wire        clk,
    input   wire        reset,

    input   wire        x_i,

    output  wire[3:0]   sr_o 
);

    logic[3:0] sr_nxt_o;
  	logic[3:0] sr_ff;

    always_ff @(posedge clk or posedge reset) begin
        if(reset) begin
            sr_ff <= 4'd0;
        end
        else begin
            sr_ff <= sr_nxt_o;
        end
    end

  assign sr_nxt_o 	= {sr_o[2:0], x_i};
  assign sr_o 		= sr_ff;
    
endmodule
