// Code your testbench here
// or browse Examples
module day15_tb();
  logic clk;
  logic reset;
  
  logic[3:0] req_i;
  logic[3:0] gnt_o;
  
  day15 DAY15(.*);
  
  always begin
    clk = 1'b0;
    #5;
    clk = 1'b1;
    #5;
  end
  
  initial begin
  	reset <= 1'b1;
    req_i <= 4'b0000;
    repeat(2) @(posedge clk);
    reset <= 1'b0;
    @(posedge clk);
    @(posedge clk);
    req_i <= 15;
//     for(int i=0; i<32; i++) begin
//       req_i <= $urandom_range(0, 4'hF);
//       @(posedge clk);
//     end
    #100 $finish();
  end
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars();
  end
  
endmodule
