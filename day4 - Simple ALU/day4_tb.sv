// Code your testbench here
// or browse Examples
module day4_tb();
  logic[7:0] a_i;
  logic[7:0] b_i;
  
  logic[2:0] op_i;
  
  logic[7:0] alu_o;
  
  day4 DAY4(.*);
  int i;
  initial begin    
    for(i = 0; i < 9; i++) begin
      a_i = $urandom_range(8'h00, 8'hFF);
      b_i = $urandom_range(8'h00, 8'hFF);
      op_i = 3'(i);
      #5;
    end
  end
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars();
    #70 $finish();    
  end
  
endmodule
